# Reescribe el programa del cálculo del salario para darle al empleado
# 1.5 veces la tarifa horaria para todas las horas
# trabajadas que excedan de 40.
# --autora-- ="Valeria Guerrero"
# --email-- ="valeria.guerrero@unl.edu.ec"
horas=int(input("Ingrese las horas"))
tarifa=float(input("Introduzca la tarifa por hora:"))
if horas  >40:
   horas_xt=horas-40
   tarifa_xt=(1.5*horas_xt)*tarifa
   salario=(40* tarifa)+ tarifa_xt
   print("Su salario es de:",salario)
else:
    salario=tarifa*horas
    print ("Su salarios es de:", salario)