# Reescribe el programa del salario usando try y except,
# de modo que el programa sea capaz de gestionar entradas no numéricas con elegancia
# mostrando un mensaje y saliendo del programa
# --autora-- = "Valeria Guerrero"
#-- email-- = "valeria.guerrero@unl.edu.ec
try:
    horas=int(input("Ingrese las horas"))
    tarifa=float(input("Introduzca la tarifa por hora:"))
    if horas  >=40:
       horas_xt=horas-40
       tarifa_xt=(0.5*tarifa)+tarifa
       salario=(tarifa_xt * horas_xt)+(horas* tarifa)
       print("Su salario es de:",salario)
    else:
        salario= horas * tarifa
        print ("Su salarios es de:", salario)
except:
    print("Error, por favor ingrese un número")