# Programa que presente una calificación cualitativa
# de acuerdo a ciertos equvalentes
# --autora-- ="Valeria Guerrero"
# --email-- ="valeria.guerrero@unl.edu.ec"
try:
    puntuación =float(input("Introduzca puntuación:"))

    # validación del rango de la puntuación
    if puntuación >= 0 and puntuación <=1.0:
      if puntuación >=0.9:
         print("sobresaliente")
      elif puntuación >=0.8:
        print("notable")
      elif puntuación >=0.7:
        print("bien")
      elif puntuación >=0.6:
        print("suficiente")
      elif puntuación >=0.6:
        print("insuficiente")
    else:
        print("Puntuación incorrecta")
except:
       print("puntuación incorrecta")